// menu
const showMenu = (toggleId, navId) => {
  const toggle = document.getElementById(toggleId),
    nav = document.getElementById(navId);
  if (toggle && nav) {
    toggle.addEventListener('click', () => {
      nav.classList.toggle('show');
    });
  }
};
showMenu('menu-toggle', 'nav');

// menu spy
const scrollSpy = () => {
  const sections = document.querySelectorAll('section[data-scroll-spy]');
  //   instansiamos nuestro objeto observer
  const observer = new IntersectionObserver(
    (entries) => {
      entries.forEach((entry) => {
        const entryId = entry.target.id;
        // si 'isIntersecting' es verdadero haz lo siguiente
        if (entry.isIntersecting) {
          document
            .querySelector(`a[data-scroll-spy][href="#${entryId}"]`)
            .classList.add('menu__link--active');
        } else {
          document
            .querySelector(`a[data-scroll-spy][href="#${entryId}"]`)
            .classList.remove('menu__link--active');
        }
      });
    },
    {
      // root
      // rootMargin
      threshold: 0.5,
    }
  );
  sections.forEach((element) => {
    observer.observe(element);
  });
};

scrollSpy();

// lazy load gallery

const loadGallery = () => {
  const gallery = document.getElementById('gallery');
  const templateGallery = document.getElementById('template-gallery').content;
  const fragment = document.createDocumentFragment();

  const peticion = async () => {
    const key = 'Hz4ir6YwMV_Utns248vWLE0V2F-_aZUKAfu39PJaOy0';
    const api = `https://api.unsplash.com/photos/?client_id=${key}&per_page=12`;
    const request = await fetch(api);
    const json = await request.json();

    console.log(json)

    for (const photo of json) {
      const image = templateGallery.querySelector('.gallery__img');
      image.src = photo.urls.raw + '&w=350';
      image.setAttribute('width', '350');
      // templateGallery.querySelector('.gallery__img').setAttribute(width, '350');

      templateGallery.querySelector('.gallery__item p').textContent =
        photo.user.first_name;

      templateGallery.querySelector('.gallery__item .likes').textContent =
        photo.likes;

      const clone = templateGallery.cloneNode(true);
      fragment.append(clone);
    }
    gallery.append(fragment);
    setObserver();
  };

  const setObserver = () => {
    const observer = new IntersectionObserver(
      (entries) => {
        for (const entry of entries) {
          if (entry.isIntersecting) {
            peticion();
            observer.unobserve(entry.target);
          }
        }
      },
      {
        threshold: 0.5,
      }
    );
    observer.observe(gallery.lastElementChild);
  };

  peticion();
};
loadGallery()
// validacion del formulario
const contactFormValidations = () => {
  const _inputs = document.querySelectorAll('.form [required]');

  for (const _input of _inputs) {
    const span = document.createElement('span');
    span.id = _input.name;
    span.textContent = _input.title;
    span.classList.add('form__error', 'form__none');
    _input.after(span);
  }
  document.addEventListener('keyup', (event) => {
    if (event.target.matches('.form [required]')) {
      let pattern = event.target.pattern || event.target.dataset.pattern;
      let spanId = document.getElementById(event.target.name);

      // validamos si tiene pattern
      if (pattern && event.target.value !== '') {
        const regex = new RegExp(pattern);
        return !regex.exec(event.target.value)
          ? spanId.classList.add('form__is-active')
          : spanId.classList.remove('form__is-active');
      }

      // validamos si  no tiene pattern
      if (!pattern) {
        return (event.target.value = ''
          ? spanId.classList.add('is-active')
          : spanId.classList.remove('is-active'));
      }
    }
  });
};

contactFormValidations();
